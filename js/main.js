(function () {
    "use strict";

    APP.Router = Backbone.Router.extend({

        routes: {
            '': 'home',
            'cars/new': 'newCar',
            'cars/:id': 'carDetails'
        },

        home: function () {
            var self = this;

            if (APP.carView) { APP.carView.close(); }
            if (APP.repairsListView) { APP.repairsListView.close(); }

            if (!APP.headerView) {
                APP.headerView = new APP.Views.Header();
                APP.headerView.render();
            }

            if (!APP.carListCollection) {
                APP.carListCollection = new APP.Cols.CarList();
                APP.carListCollection.fetch({
                    success: function () {
                        APP.carListView = new APP.Views.CarList({
                            collection: APP.carListCollection
                        });
                        APP.carListView.render();

                        if (self.requestedId) {
                            self.carDetails(self.requestedId);
                        }
                    }
                });
            }
        },

        carDetails: function (id) {
            if (APP.carListCollection) {
                APP.car = APP.carListCollection.get(id);

                if (APP.carView) { APP.carView.close(); }

                APP.carView = new APP.Views.CarDetails({
                    model: APP.car
                });

                APP.carView.render();

                APP.carRepairs = new APP.Cols.RepairList([], { id: id });
                APP.carRepairs.fetch();

                if (APP.repairsListView) { APP.repairsListView.close(); }

                APP.repairsListView = new APP.Views.RepairList({
                    collection: APP.carRepairs
                });

            } else {
                this.requestedId = id;
                this.home();
            }
        },

        newCar: function () {
            if (APP.carView) { APP.carView.close(); }
            if (APP.repairsListView) { APP.repairsListView.close(); }

            if (!APP.headerView) {
                APP.headerView = new APP.Views.Header();
                APP.headerView.render();
            }

            if (!APP.carListCollection) {
                APP.carListCollection = new APP.Cols.CarList();
                APP.carListCollection.fetch({
                    success: function () {
                        APP.carListView = new APP.Views.CarList({
                            collection: APP.carListCollection
                        });
                        APP.carListView.render();
                    }
                });
            }

            APP.carView = new APP.Views.CarDetails({
                model: new APP.Models.Car()
            });

            APP.carView.render();
        }

    });

    APP.utils.tpl.loadTemplates(['header', 'preloader', 'car_details', 'car_list', 'car_repair_details', 'car_repair_list'], function () {

        // start the app
        APP.carAppRouter = new APP.Router();
        Backbone.history.start();

    });
}());