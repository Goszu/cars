(function () {
    "use strict";

    APP.utils.tpl = {
        // Hash of preloaded templates for the app
        templates: {},

        translate: function (templateMarkup, lang) {
            var replacer = function (match, m1) {
                return APP.TRN[lang][m1];
            };
            return templateMarkup.replace(/TRN\.(\w+)/g, replacer);
        },

        // Recursively pre-load all the templates for the app.
        loadTemplates: function (names, callback) {
            var that = this, loadTemplate;

            loadTemplate = function (index) {
                var name = names[index];
                $.get('tpl/' + name + '.html', function (data) {
                    that.templates[name] = that.translate(data, APP.lang);
                    index += 1;
                    if (index < names.length) {
                        loadTemplate(index);
                    } else {
                        callback();
                    }
                });
            };

            loadTemplate(0);
        },

        // Get template by name from hash of preloaded templates
        get: function (name) {
            return this.templates[name];
        }
    };
}());