(function () {
    "use strict";

    APP.Views.BaseView = Backbone.View.extend({
        showPreloader: function () {
            var that = this;

            this.preloaderTimeout = setTimeout(function () {
                that.$el.html(APP.utils.tpl.get('preloader'));
            }, 500);
        },

        close: function () {
            $(this.el).unbind();
            $(this.el).empty();
        }
    });
}());