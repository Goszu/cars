(function () {
    "use strict";

    APP.Views.RepairListItem = APP.Views.BaseView.extend({
        tagName: 'article',

        className: "repair-details closed",

        initialize: function () {
            this.template = _.template(APP.utils.tpl.get('car_repair_details'));
            this.model.on('change', this.render, this);
            this.model.on('destroy', this.close, this);
        },

        render: function () {
            var attributes = this.model.toJSON();
            this.$el.html(this.template(attributes));
            return this;
        },

        close: function () {
            this.$el.unbind();
            this.$el.remove();
        },

        events: {
            "click": "expand",
            "click .close": "collapse",
            "click .save": "saveRepair",
            "click .delete": "deleteRepair",
            "keydown input": "markAsChanged",
            "keydown textarea": "markAsChanged"
        },

        expand: function () {
            var $txtArea = this.$('textarea');

            if (this.$el.hasClass('closed')) {
                this.$el.removeClass('closed');
                this.$('input').removeAttr('disabled');
                $txtArea.removeAttr('disabled');
                if (!$txtArea.expanding('active')) {
                    $txtArea.expanding();
                }
                this.$('button').removeAttr('disabled');
            }
        },

        collapse: function (e) {
            this.$el.addClass('closed');
            this.$('input').attr('disabled', 'disabled');
            this.$('textarea').attr('disabled', 'disabled');
            this.$('button').attr('disabled', 'disabled');
            e.stopPropagation();
        },

        saveRepair: function () {
            this.model.saveRepair(this);
        },

        deleteRepair: function () {
            if (confirm(APP.utils.tpl.translate('TRN.confirm_repair_delete', APP.lang))) {
                this.model.destroy();
            }
        },

        markAsChanged: function (e) {
            if (e.keyCode !== 9 && e.keyCode !== 27) {
                $(e.target).parent().addClass('has-warning');
            }
        },

        unmarkAll: function (wasNewItem) {
            this.$('.has-warning').removeClass('has-warning');
            if (wasNewItem) { this.switchButtonText(); }
        },

        switchButtonText: function () {
            this.$('button.save').text(APP.utils.tpl.translate('TRN.update', APP.lang));
        }
    });

    APP.Views.RepairList = APP.Views.BaseView.extend({
        initialize: function () {
            this.template = _.template(APP.utils.tpl.get('car_repair_list'));
            this.collection.on("reset", this.render, this);
            this.showPreloader();
        },

        el: "#car-repairs",

        events: {
            'click #add-repair': 'addOneOnTop',
            'click .glyphicon-resize-small': 'collapseAll',
            'click .glyphicon-resize-full': 'expandAll'
        },

        render: function () {
            clearTimeout(this.preloaderTimeout);
            this.$el.empty().hide();
            this.$el.html(this.template());
            this.addAll();
            this.$el.fadeIn(250);
            return this;
        },

        addOne: function (repair) {
            var repairListItem = new APP.Views.RepairListItem({model: repair});
            repairListItem.render();
            this.$('section').append(repairListItem.el);
        },

        addOneOnTop: function () {
            var repairListItem = new APP.Views.RepairListItem({model: new APP.Models.Repair()});
            repairListItem.render();
            this.$('section').prepend(repairListItem.el);
            repairListItem.$el.hide();
            repairListItem.expand();
            repairListItem.$("button.save").text(APP.utils.tpl.translate('TRN.add', APP.lang));
            repairListItem.$el.fadeIn(400, function () {
                repairListItem.$("textarea").expanding();
            });
        },

        addAll: function () {
            this.collection.forEach(this.addOne, this);
        },

        collapseAll: function () {
            this.$('article').addClass('closed');
            this.$('article input').attr('disabled', 'disabled');
            this.$('article textarea').attr('disabled', 'disabled');
            this.$('article button').attr('disabled', 'disabled');
        },

        expandAll: function () {
            var $txtArea = this.$('article textarea');

            this.$('article').removeClass('closed');
            this.$('article input').removeAttr('disabled');
            $txtArea.removeAttr('disabled');
            if (!$txtArea.expanding('active')) {
                $txtArea.expanding();
            }
            this.$('article button').removeAttr('disabled');
        },

        close: function () {
            this.$('textarea.details').expanding('destroy');
            $(this.el).unbind();
            $(this.el).empty();
        }
    });
}());