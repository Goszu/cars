APP.Views.CarListItem = APP.Views.BaseView.extend({
    tagName: 'a',
    className: 'list-group-item',
    attributes: function () {
        var id = this.model.get('id');

        return {
            'data-id': id,
            'href': '#cars/' + id
        };
    },
    render: function () {
        this.$el.text(this.model.get('reg'));
        return this;
    }
});

APP.Views.CarList = APP.Views.BaseView.extend({
    initialize: function () {
        this.template = _.template(APP.utils.tpl.get('car_list'));
        this.collection.on("reset", this.render, this);
        this.collection.on("add", this.render, this);
        this.collection.on("change", this.render, this);
        this.collection.on("destroy", this.render, this);
    },

    el: "#cars-list",

    events: {
        'click a': 'highlight'
    },

    render: function () {
        this.$el.empty().hide();
        this.$el.html(this.template());
        this.listEl = this.$el.find('ul');
        this.addAll();
        this.$el.fadeIn(250);
        return this;
    },

    addOne: function (car) {
        var carListItem = new APP.Views.CarListItem({model: car});
        carListItem.render();
        this.listEl.append(carListItem.el);
    },

    addAll: function () {
        this.collection.forEach(this.addOne, this);
    },

    highlight: function (e) {
        $('#cars-list .active').removeClass('active');
        $(e.target).addClass('active');
    },

    close: function () {
        $(this.el).unbind();
        $(this.el).empty();
    }
});
