APP.Views.Header = APP.Views.BaseView.extend({
    initialize: function () {
        this.template = _.template(APP.utils.tpl.get('header'));
    },

    el: ".app-header",

    render: function () {
        this.$el.html(this.template());
        return this;
    },

    events: {
        'click button.add-car': 'addVehicle'
    },

    addVehicle: function () {
        APP.carAppRouter.navigate('cars/new', true);
    },

    close: function () {
        this.$el.unbind();
        this.$el.empty();
    }

});