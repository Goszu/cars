APP.Views.CarDetails = APP.Views.BaseView.extend({
    initialize: function () {
        this.template = _.template(APP.utils.tpl.get('car_details'));
        this.model.on('change', this.render, this);
        this.model.on('destroy', this.close, this);
    },

    el: "#car-details",

    render: function () {
        var attributes = this.model.toJSON();
        this.$el.hide();
        this.$el.html(this.template(attributes));
        this.$el.fadeIn(400);
        if (APP.carListView) {
            APP.carListView.$el.find('.active').removeClass('active');
            APP.carListView.$el.find('[data-id=' + attributes.id + ']').addClass('active');
        }
        return this;
    },

    events: {
        'click #save': 'saveCar',
        'click #delete': 'deleteCar',
        'keydown input': 'markAsChanged'
    },

    saveCar: function () {
        this.model.saveCar();
    },

    deleteCar: function () {
        if (confirm(APP.utils.tpl.translate('TRN.confirm_car_delete', APP.lang))) {
            this.model.destroy({
                success: function () {
                    APP.carAppRouter.navigate('', true);
                }
            });
        }
    },

    markAsChanged: function (e) {
        if (e.keyCode !== 9 && e.keyCode !== 27) {
            $(e.target).parent().addClass('has-warning');
        }
    },

    close: function () {
        this.$el.unbind();
        this.$el.empty();
    }

});