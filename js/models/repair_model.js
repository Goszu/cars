APP.Models.Repair = Backbone.Model.extend({
    urlRoot: "../api/repairs",
    defaults: function () {
        var d = new Date(),
            dateString = d.getFullYear() + "-" + (d.getMonth() + 1) + "-" + d.getDate();

        return {
            date: dateString,
            details: "",
            mileage: ""
        };
    },

    saveRepair: function (thisView) {
        var isNewItem = this.isNew();

        // as a good habit - adding model to collection (although referencing collection globally is not recommended)
        // without this, syncing would work as well - even though model is not added to the current car repairs list,
        // it's properly saved to the database (with the current car id), and would be added to the collection with next fetch()
        if (isNewItem && APP.carRepairs) {
            APP.carRepairs.add(this);
        }

        this.save({
            car_id: APP.car.get('id'),
            date: thisView.$el.find('.repair-date').val(),
            details: thisView.$el.find('.details').val(),
            mileage: thisView.$el.find('.mileage').val()
        }, {
            success: function () {
                thisView.unmarkAll(isNewItem);
            },
            silent: true
        });
    }
});

APP.Cols.RepairList = Backbone.Collection.extend({
    initialize: function (models, options) {
        this.url = 'api/repairs/' + options.id;
    },
    model: APP.Models.Repair
});