APP.Models.Car = Backbone.Model.extend({
    urlRoot: 'api/cars',
    defaults: function () {
        return {
            reg: "",
            make: "",
            model: "",
            prodyear: "",
            power: "",
            vin: "",
            inspection: "",
            insurance: ""
        };
    },

    saveCar: function () {
        this.set({
            reg: $('#reg').val(),
            make: $('#make').val(),
            model: $('#model').val(),
            prodyear: $('#prodyear').val(),
            power: $('#power').val(),
            vin: $('#vin').val(),
            inspection: $('#inspection').val(),
            insurance: $('#insurance').val()
        }, {silent: true});

        if (this.isNew()) {
            var that = this;
            APP.carListCollection.create(this, {
                success: function () {
                    APP.carAppRouter.navigate('cars/' + that.id, true);
                },
                wait: true
            });
        } else {
            this.save(this, {wait: true});
        }
    }
});

APP.Cols.CarList = Backbone.Collection.extend({
    model: APP.Models.Car,
    url: 'api/cars',
    comparator: function (car) {
        return car.get('reg');
    }
});