APP.TRN.pl = {
    //Common Buttons
    add: 'dodaj',
    save: 'zapisz',
    update: 'aktualizuj',
    del: 'usuń',

    //APP.Views.Header
    add_vehicle: 'Dodaj pojazd',

    //APP.Views.CarDetails
    reg_num: 'Numer rejestracyjny',
    make: 'Marka',
    model: 'Model',
    prod_year: 'Rok produkcji',
    power: 'Moc',
    vin: 'VIN',
    inspection: 'Termin przeglądu',
    insurance: 'Termin ubezpieczenia',
    confirm_car_delete: 'Czy napewno chcesz usunąć ten pojazd?',

    //APP.Views.RepairList
    repairs: 'Naprawy',
    date: 'Data',
    mileage: 'Przebieg',
    confirm_repair_delete: 'Czy napewno chcesz usunąć tą naprawę?'
};