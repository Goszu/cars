APP.TRN.en = {
    //Common Buttons
    add: 'add',
    save: 'save',
    update: 'update',
    del: 'delete',

    //APP.Views.Header
    add_vehicle: 'Add a vehicle',

    //APP.Views.CarDetails
    reg_num: 'Registration number',
    make: 'Make',
    model: 'Model',
    prod_year: 'Production year',
    power: 'Power',
    vin: 'VIN',
    inspection: 'NCT expires',
    insurance: 'Insurance expires',
    confirm_car_delete: 'Do you really want to delete this car?',

    //APP.Views.RepairList
    repairs: 'Repairs',
    date: 'Date',
    mileage: 'Mileage',
    confirm_repair_delete: 'Do you really want to delete this repair entry?'
};