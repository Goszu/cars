<?php

require 'Slim/Slim.php';

$app = new Slim();

function raiseException($e) {
    global $app;
    $app->response()->status(500);
    echo '{"error":{"text":'. $e->getMessage() .'}}';
}

$app->get('/cars/', 'getCars');
$app->get('/cars/:id', 'getCarDetails');
$app->get('/repairs/:id', 'getCarRepairs');
$app->put('/cars/:id', 'updateCarDetails');
$app->put('/repairs/:id', 'updateCarRepairDetails');
$app->post('/repairs', 'addCarRepair');
$app->post('/cars', 'addCar');
$app->delete('/cars/:id', 'deleteCar');
$app->delete('/repairs/:id', 'deleteRepair');

$app->run();

function getCars() {

    $sql = "select c.id, c.reg, c.make, c.model, c.prodyear, c.power, c.vin " .
    		"from cars c " .
    		"order by c.reg";
	try {
		$db = getConnection();
		$stmt = $db->query($sql);
		$cars = $stmt->fetchAll(PDO::FETCH_OBJ);
		$db = null;

        // Include support for JSONP requests
        if (!isset($_GET['callback'])) {
            echo json_encode($cars);
        } else {
            echo $_GET['callback'] . '(' . json_encode($cars) . ');';
        }

	} catch(PDOException $e) {
        raiseException($e);
	}
}

function getCarDetails($id) {
    $sql = "SELECT c.id, c.reg, c.make, c.model, c.prodyear, c.power, c.vin " .
			"FROM cars c " .
    		"WHERE c.id=:id";
	try {
		$db = getConnection();
		$stmt = $db->prepare($sql);
		$stmt->bindParam("id", $id);
		$stmt->execute();
		$car = $stmt->fetchObject();
		$db = null;

        // Include support for JSONP requests
        if (!isset($_GET['callback'])) {
            echo json_encode($car);
        } else {
            echo $_GET['callback'] . '(' . json_encode($car) . ');';
        }

	} catch(PDOException $e) {
        raiseException($e);
	}
}

function updateCarDetails($id) {
    $request = Slim::getInstance()->request();
    $body = $request->getBody();
    $car = json_decode($body);
    $sql = "UPDATE cars SET reg=:reg, make=:make, model=:model, prodyear=:prodyear, power=:power, vin=:vin WHERE id=:id";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $car->id);
        $stmt->bindParam("reg", $car->reg);
        $stmt->bindParam("make", $car->make);
        $stmt->bindParam("model", $car->model);
        $stmt->bindParam("prodyear", $car->prodyear);
        $stmt->bindParam("power", $car->power);
        $stmt->bindParam("vin", $car->vin);
        $stmt->execute();
        $db = null;
        echo json_encode($car);
        //echo $sql;
        //echo $stmt;
    } catch(PDOException $e) {
        raiseException($e);
    }
}

function updateCarRepairDetails($id) {
    $request = Slim::getInstance()->request();
    $body = $request->getBody();
    $repair = json_decode($body);

    $sql = "UPDATE repairs SET car_id=:car_id, date=:date, details=:details, mileage=:mileage WHERE id=:id";

    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $repair->id);
        $stmt->bindParam("car_id", $repair->car_id);
        $stmt->bindParam("date", $repair->date);
        $stmt->bindParam("details", $repair->details);
        $stmt->bindParam("mileage", $repair->mileage);
        $stmt->execute();
        $db = null;
        echo json_encode($repair);
        //echo $sql;
        //echo $stmt;
    } catch(PDOException $e) {
        raiseException($e);
    }
}

function getCarRepairs($id) {
    $sql = "SELECT r.id, r.car_id, r.date, r.details, r.mileage " .
        "FROM repairs r " .
        "WHERE r.car_id=:id " .
        "ORDER BY r.date DESC";
    try {


        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $repairs = $stmt->fetchAll(PDO::FETCH_OBJ);
        $db = null;

        // Include support for JSONP requests
        if (!isset($_GET['callback'])) {
            echo json_encode($repairs);
        } else {
            echo $_GET['callback'] . '(' . json_encode($repairs) . ');';
        }

    } catch(PDOException $e) {
        raiseException($e);
    }
}

function addCarRepair() {
    $request = Slim::getInstance()->request();
    $repair = json_decode($request->getBody());
    $sql = "INSERT INTO repairs (car_id, date, mileage, details) VALUES (:car_id, :date, :mileage, :details)";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("car_id", $repair->car_id);
        $stmt->bindParam("date", $repair->date);
        $stmt->bindParam("mileage", $repair->mileage);
        $stmt->bindParam("details", $repair->details);
        $stmt->execute();
        $repair->id = $db->lastInsertId();
        $db = null;
        echo json_encode($repair);
    } catch(PDOException $e) {
        raiseException($e);
    }
}

function addCar() {
    $request = Slim::getInstance()->request();
    $car = json_decode($request->getBody());
    $sql = "INSERT INTO cars (reg, make, model, prodyear, power, vin) VALUES (:reg, :make, :model, :prodyear, :power, :vin)";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("reg", $car->reg);
        $stmt->bindParam("make", $car->make);
        $stmt->bindParam("model", $car->model);
        $stmt->bindParam("prodyear", $car->prodyear);
        $stmt->bindParam("power", $car->power);
        $stmt->bindParam("vin", $car->vin);
        $stmt->execute();
        $car->id = $db->lastInsertId();
        $db = null;
        echo json_encode($car);
    } catch(PDOException $e) {
        raiseException($e);
    }
}

function deleteCar($id) {
    $sql = "DELETE FROM cars WHERE id=:id";
    $sql2 = "DELETE FROM repairs WHERE car_id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();

        //delete repairs associated with this vehicle
        $stmt = $db->prepare($sql2);
        $stmt->bindParam("id", $id);
        $stmt->execute();

        $db = null;

    } catch(PDOException $e) {
        raiseException($e);
    }
}

function deleteRepair($id) {
    $sql = "DELETE FROM repairs WHERE id=:id";
    try {
        $db = getConnection();
        $stmt = $db->prepare($sql);
        $stmt->bindParam("id", $id);
        $stmt->execute();
        $db = null;
    } catch(PDOException $e) {
        raiseException($e);
    }
}

function getConnection() {
	$dbhost="127.0.0.1";
	$dbuser="root";
	$dbpass="root";
	$dbname="cars";
	$dbh = new PDO("mysql:host=$dbhost;dbname=$dbname", $dbuser, $dbpass);	
	$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	return $dbh;
}

?>