describe('carListView', function () {
    "use strict";

    var fakeServer, carList, carColection, carListView, tmplLoaded = false;

    beforeEach(function () {

        jasmine.getFixtures().fixturesPath = "test/fixtures";
        loadFixtures('car_list.html');

        APP.utils.tpl.loadTemplates(['car_list'], function () {
            tmplLoaded = true;
        });

        waitsFor(function () {
            return tmplLoaded;
        }, 'templates to be loaded', 1000);

        runs(function () {
            fakeServer = sinon.fakeServer.create();
            carList = [
                {id: 0, reg: 'reg0'},
                {id: 1, reg: 'reg1'},
                {id: 2, reg: 'reg2'}
            ];

            fakeServer.respondWith(
                'GET',
                'api/cars',
                [
                    200,
                    { "Content-Type": "application/json" },
                    JSON.stringify(carList)
                ]
            );

            carColection = new APP.Cols.CarList();
            carListView = new APP.Views.CarList({collection: carColection});
            carColection.fetch();

            //Respond to any queued AJAX calls
            fakeServer.respond();

            carListView.render();
        });
    });

    it('should show list of all cars', function () {
        waitsFor(function () {
            return $('#cars-list').html();
        }, 'list to get populated', 1000);

        runs(function () {
            expect($("#cars-list a").length).toEqual(3);
            fakeServer.restore();
        });
    });

    it('should highlight the selected car on the list', function () {
        waitsFor(function () {
            return $('#cars-list').html();
        }, 'list to get populated', 1000);

        runs(function () {
            $("#cars-list a:first-child").trigger('click');

            expect($("#cars-list a:first-child")).toHaveClass('active');

            $("#cars-list a:nth-child(3)").trigger('click');

            expect($("#cars-list a:first-child")).not.toHaveClass('active');
            expect($("#cars-list a:nth-child(3)")).toHaveClass('active');
            fakeServer.restore();
        });
    });

    it('should sort the list when new car added', function () {
        waitsFor(function () {
            return $('#cars-list').html();
        }, 'list to get populated', 1000);

        runs(function () {
            carColection.add(new APP.Models.Car({id: 4, reg: 'aaa'}));

            expect($("#cars-list a:first-child").text()).toEqual('aaa');

            fakeServer.restore();
        });
    });
});

describe('repairsListView', function () {
    "use strict";

    var fakeServer, repairList, repairDetailsListView, tmplLoaded = false;

    APP.lang = 'en';

    beforeEach(function () {

        jasmine.getFixtures().fixturesPath = "test/fixtures";
        loadFixtures('car_repair_list.html');

        APP.utils.tpl.loadTemplates(['car_repair_details', 'car_repair_list'], function () {
            tmplLoaded = true;
        });

        waitsFor(function () {
            return tmplLoaded;
        }, 'templates to be loaded', 1000);

        runs(function () {
            fakeServer = sinon.fakeServer.create();
            repairList = [
                {id: 0, car_id: 1, date: '2013-04-15', details: 'changing the oil'},
                {id: 1, car_id: 1, date: '2013-04-16', details: 'changing the brake pads'},
                {id: 2, car_id: 1, date: '2013-05-10', details: 'changing broken left mirror'}
            ];

            fakeServer.respondWith(
                'GET',
                'api/repairs/1',
                [
                    200,
                    { "Content-Type": "application/json" },
                    JSON.stringify(repairList)
                ]
            );

            fakeServer.respondWith(
                'PUT',
                '../api/repairs/0',
                [
                    200,
                    { "Content-Type": "text/html" },
                    '{"ok": "true"}'
                ]
            );

            fakeServer.respondWith(
                'POST',
                '../api/repairs',
                [
                    200,
                    { "Content-Type": "text/html" },
                    '{"ok": "true"}'
                ]
            );

            APP.carRepairs = new APP.Cols.RepairList([], {id: 1});
            repairDetailsListView = new APP.Views.RepairList({collection: APP.carRepairs});
            APP.carRepairs.fetch();

            //Respond to any queued AJAX calls
            fakeServer.respond();

            repairDetailsListView.render();
        });
    });

    it('should show list of all repairs for a specified car', function () {
        waitsFor(function () {
            return $('#car-repairs').html();
        }, 'list to get populated', 1000);

        runs(function () {
            expect($("#car-repairs article").length).toEqual(3);
            fakeServer.restore();
        });
    });

    it('should show new repair entry, when "new repair entry" button clicked', function () {
        waitsFor(function () {
            return $('#car-repairs').html();
        }, 'list to get populated', 1000);

        runs(function () {
            var d = new Date(),
                curr_date = d.getDate(),
                curr_month = d.getMonth() + 1,
                curr_year = d.getFullYear(),
                dateString = curr_year + "-" + curr_month + "-" + curr_date;

            spyOn(APP.utils.tpl, "translate");

            $("#add-repair").trigger('click');
            expect($("#car-repairs article").length).toEqual(4);
            expect($("#car-repairs article:first-child input").val()).toEqual(dateString);
            expect($("#car-repairs article:first-child")).not.toHaveClass('closed');
            expect($("#car-repairs article:first-child .repair-date")).not.toHaveAttr('disabled');
            expect($("#car-repairs article:first-child .mileage")).not.toHaveAttr('disabled');
            expect(APP.utils.tpl.translate).toHaveBeenCalledWith('TRN.add', 'en');
            expect(APP.carRepairs.length).toEqual(3);
            fakeServer.restore();
        });
    });

    it('should save the new entry when save button clicked', function () {
        waitsFor(function () {
            return $('#car-repairs').html();
        }, 'list to get populated', 1000);

        runs(function () {

            // Save repair gets id from currently "opened" car - trough APP.car.get('id'). We need to stub it.
            APP.car = {};
            APP.car.get = jasmine.createSpy("APP.car.get() spy").andReturn(1);

            $("#add-repair").trigger('click');
            $("#car-repairs article:first-child textarea").text('details text');

            spyOn(APP.utils.tpl, "translate");

            $("#car-repairs article:first-child button.save").trigger('click');
            fakeServer.respond();

            expect(APP.carRepairs.length).toEqual(4);
            expect(APP.carRepairs.models[3].get('details')).toEqual('details text');
            expect(APP.utils.tpl.translate).toHaveBeenCalledWith('TRN.update', 'en');
            expect($("#car-repairs article:first-child .repair-date")).not.toHaveAttr('disabled');
            expect($("#car-repairs article:first-child .mileage")).not.toHaveAttr('disabled');
            fakeServer.restore();
        });
    });

    it('should remove the repair entry when delete button clicked', function () {
        waitsFor(function () {
            return $('#car-repairs').html();
        }, 'list to get populated', 1000);

        runs(function () {
            repairDetailsListView.expandAll();

            $("#car-repairs article:first-child button.delete").trigger('click');

            expect(APP.carRepairs.length).toEqual(2);
            fakeServer.restore();
        });
    });

    it('should mark changed and unsaved input fields and unmark them if save button clicked', function () {
        waitsFor(function () {
            return $('#car-repairs').html();
        }, 'view to get populated', 1000);

        runs(function () {
            var $first = $('.list article:first-child');

            $first.trigger('click');

            $first.find('.repair-date').trigger('keydown');
            $first.find('.mileage').trigger('keydown');
            $first.find('.details').trigger('keydown');

            expect($first.find('.repair-date').parent()).toHaveClass('has-warning');
            expect($first.find('.mileage').parent()).toHaveClass('has-warning');
            expect($first.find('.details').parent()).toHaveClass('has-warning');

            $first.find('.save').trigger('click');
            fakeServer.respond();

            expect($first.find('.repair-date').parent()).not.toHaveClass('has-warning');
            expect($first.find('.mileage').parent()).not.toHaveClass('has-warning');
            expect($first.find('.details').parent()).not.toHaveClass('has-warning');

            fakeServer.restore();
        });
    });

    it('expand and collapse all repair entries when apropriate controls clicked', function () {
        waitsFor(function () {
            return $('#car-repairs').html();
        }, 'view to get populated', 1000);

        runs(function () {
            $('.glyphicon-resize-full').trigger('click');

            expect($("#car-repairs article.closed").length).toEqual(0);
            expect($("#car-repairs article.closed input[disabled=disabled]").length).toEqual(0);

            $('.glyphicon-resize-small').trigger('click');

            expect($("#car-repairs article.closed").length).toEqual(3);
            expect($("#car-repairs article.closed input[disabled=disabled]").length).toEqual(6);

            fakeServer.restore();
        });
    });
});

describe('APP.Views.CarDetails - integration', function () {
    "use strict";

    var fakeServer, tmplLoaded = false;

    beforeEach(function () {

        jasmine.getFixtures().fixturesPath = "test/fixtures";
        loadFixtures('car_repair_list.html');

        APP.utils.tpl.loadTemplates(['car_details'], function () {
            tmplLoaded = true;
        });

        waitsFor(function () {
            return tmplLoaded;
        }, 'templates to be loaded', 1000);

        runs(function () {
            fakeServer = sinon.fakeServer.create();

            fakeServer.respondWith(
                'POST',
                'api/cars',
                [
                    200,
                    { "Content-Type": "text/html" },
                    '{"ok": "true"}'
                ]
            );

            var carView = new APP.Views.CarDetails({
                model: new APP.Models.Car()
            });

            carView.render();

            APP.carListCollection = new APP.Cols.CarList();

            // saveCar method in the car model uses router. We need to stub it.
            APP.carAppRouter = {};
            APP.carAppRouter.navigate = jasmine.createSpy("APP.carAppRouter.navigate() spy").andReturn(true);
        });
    });


    it('should mark changed and unsaved input fields and unmark them if save button clicked', function () {
        waitsFor(function () {
            return $('#car-details').html();
        }, 'view to get populated', 1000);

        runs(function () {
            $('#reg').trigger('keydown');
            $('#make').trigger('keydown');
            $('#model').trigger('keydown');
            $('#prodyear').trigger('keydown');
            $('#power').trigger('keydown');
            $('#vin').trigger('keydown');
            $('#inspection').trigger('keydown');
            $('#insurance').trigger('keydown');

            expect($('#reg').parent()).toHaveClass('has-warning');
            expect($('#make').parent()).toHaveClass('has-warning');
            expect($('#model').parent()).toHaveClass('has-warning');
            expect($('#prodyear').parent()).toHaveClass('has-warning');
            expect($('#power').parent()).toHaveClass('has-warning');
            expect($('#vin').parent()).toHaveClass('has-warning');
            expect($('#inspection').parent()).toHaveClass('has-warning');
            expect($('#insurance').parent()).toHaveClass('has-warning');

            $('#save').trigger('click');
            fakeServer.respond();

            expect(APP.carAppRouter.navigate).toHaveBeenCalled();
            expect($('#reg').parent()).not.toHaveClass('has-warning');
            expect($('#make').parent()).not.toHaveClass('has-warning');
            expect($('#model').parent()).not.toHaveClass('has-warning');
            expect($('#prodyear').parent()).not.toHaveClass('has-warning');
            expect($('#power').parent()).not.toHaveClass('has-warning');
            expect($('#vin').parent()).not.toHaveClass('has-warning');
            expect($('#inspection').parent()).not.toHaveClass('has-warning');
            expect($('#insurance').parent()).not.toHaveClass('has-warning');

            fakeServer.restore();
        });
    });
});