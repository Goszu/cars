describe('APP.utils.tpl.translate', function () {
    "use strict";

    var lang = 'pl',
        template = '<a href="http://link.com">TRN.page</a><p>TRN.link_to_page</p>';

    APP.TRN = {
        pl: {
            page: 'strona',
            link_to_page: "link do strony"
        },
        en: {
            page: 'page',
            link_to_page: "link to page"
        }
    };

    beforeEach(function () {

    });

    it('should translate template to pl', function () {
        var template2 = APP.utils.tpl.translate(template, 'pl');

        expect(template2).toEqual('<a href="http://link.com">strona</a><p>link do strony</p>');
    });


    it('should translate template to en', function () {
        var template2 = APP.utils.tpl.translate(template, 'en');

        expect(template2).toEqual('<a href="http://link.com">page</a><p>link to page</p>');
    });
});