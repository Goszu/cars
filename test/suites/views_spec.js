describe('APP.Views.CarDetails', function () {
    "use strict";

    var carModel = {
            id: "3",
            reg: "GKA0001",
            make: "Skoda",
            model: "Octavia 1.8T",
            prodyear: "2008",
            power: "180",
            vin: "1234",
            inspection: "2015/03/20",
            insurance: "2015/04/20"
        },
        carView,
        model = {
            toJSON: function () { return carModel; },
            on: function () {}
        },
        tmplLoaded = false;

    beforeEach(function () {
        jasmine.getFixtures().fixturesPath = "test/fixtures";
        loadFixtures('car_details.html');

        APP.utils.tpl.loadTemplates(['car_details'], function () {
            tmplLoaded = true;
        });

        waitsFor(function () {
            return tmplLoaded;
        }, 'templates to be loaded', 1000);

        runs(function () {
            spyOn(APP.Views.CarDetails.prototype, 'saveCar');
            spyOn(APP.Views.CarDetails.prototype, 'deleteCar');

            carView = new APP.Views.CarDetails({
                model: model
            });

            carView.render();
        });
    });

    it('should print out car details', function () {
        waitsFor(function () {
            return $('#car-details').html();
        }, 'view to get populated', 1000);

        runs(function () {
            expect($('#reg')).toHaveValue('GKA0001');
            expect($('#make')).toHaveValue('Skoda');
            expect($('#model')).toHaveValue('Octavia 1.8T');
            expect($('#prodyear')).toHaveValue('2008');
            expect($('#power')).toHaveValue('180');
            expect($('#vin')).toHaveValue('1234');
            expect($('#inspection')).toHaveValue('2015/03/20');
            expect($('#insurance')).toHaveValue('2015/04/20');
        });
    });

    it('should call save and delete action when appropriate buttons clicked', function () {
        waitsFor(function () {
            return $('#car-details').html();
        }, 'view to get populated', 1000);

        runs(function () {
            $('#save').trigger('click');
            $('#delete').trigger('click');

            expect(carView.saveCar).toHaveBeenCalled();
            expect(carView.deleteCar).toHaveBeenCalled();
        });
    });
});

describe('APP.Views.RepairListItem', function () {
    "use strict";

    var repairModel = {
            id: "1",
            car_id: "1",
            date: "2013-04-20",
            details: "Changed brake pads",
            mileage: "99990"
        },
        repairDetailsView,
        model = {
            toJSON: function () { return repairModel; },
            on: function () {}
        },
        tmplLoaded = false;

    // stubbing expanding plugin
    $.fn.expanding = function () {};
    spyOn($.fn, 'expanding');

    beforeEach(function () {
        jasmine.getFixtures().fixturesPath = "test/fixtures";
        loadFixtures('car_repair.html');

        APP.utils.tpl.loadTemplates(['car_repair_details'], function () {
            tmplLoaded = true;
        });

        waitsFor(function () {
            return tmplLoaded;
        }, 'templates to be loaded', 1000);

        runs(function () {
            spyOn(APP.Views.RepairListItem.prototype, 'saveRepair');
            spyOn(APP.Views.RepairListItem.prototype, 'deleteRepair');

            repairDetailsView = new APP.Views.RepairListItem({
                model: model
            });

            $('.car-repairs').html(repairDetailsView.render().el);
        });
    });

    it('should create repair details item', function () {
        waitsFor(function () {
            return $('.car-repairs').html();
        }, 'view to get populated', 1000);

        expect($('.repair-details')).toExist();
        expect($('.repair-details')).toHaveClass('closed');
        expect($('.repair-date')).toHaveAttr('disabled', 'disabled');
        expect($('.mileage')).toHaveAttr('disabled', 'disabled');
        expect($('button.close')).toExist();
    });

    it('should expand collapsed details item, when clicked', function () {
        waitsFor(function () {
            return $('.car-repairs').html();
        }, 'view to get populated', 1000);

        $('.repair-details').trigger('click');

        expect($('.repair-details')).not.toHaveClass('closed');
        expect($('.repair-date')).not.toHaveAttr('disabled', 'disabled');
        expect($('.mileage')).not.toHaveAttr('disabled', 'disabled');
        expect($.fn.expanding).toHaveBeenCalled();
    });

    it('should collapse expanded details item, when close button clicked', function () {
        waitsFor(function () {
            return $('.car-repairs').html();
        }, 'view to get populated', 1000);

        $('.repair-details').trigger('click');
        $('.close').trigger('click');

        expect($('.repair-details')).toHaveClass('closed');
        expect($('.repair-date')).toHaveAttr('disabled', 'disabled');
        expect($('.mileage')).toHaveAttr('disabled', 'disabled');
    });

    it('should print out repair details', function () {
        waitsFor(function () {
            return $('.car-repairs').html();
        }, 'view to get populated', 1000);

        runs(function () {
            expect($('.repair-details input')).toHaveValue('2013-04-20');
            expect($('.details')).toHaveValue('Changed brake pads');
            expect($('.mileage')).toHaveValue('99990');
        });
    });

    it('should call save and delete action when appropriate buttons clicked', function () {
        waitsFor(function () {
            return $('.car-repairs').html();
        }, 'view to get populated', 1000);

        runs(function () {
            repairDetailsView.expand();

            $('.save').trigger('click');
            $('.delete').trigger('click');

            expect(repairDetailsView.saveRepair).toHaveBeenCalled();
            expect(repairDetailsView.deleteRepair).toHaveBeenCalled();
        });
    });
});

describe('APP.Views.Header', function () {
    "use strict";

    var tmplLoaded = false, headerView;

    beforeEach(function () {
        jasmine.getFixtures().fixturesPath = "test/fixtures";
        loadFixtures('header.html');

        APP.utils.tpl.loadTemplates(['header'], function () {
            tmplLoaded = true;
        });

        waitsFor(function () {
            return tmplLoaded;
        }, 'templates to be loaded', 1000);

        runs(function () {
            spyOn(APP.Views.Header.prototype, 'addVehicle');

            headerView = new APP.Views.Header();
            headerView.render();
        });
    });

    it('should render add-car button', function () {
        waitsFor(function () {
            return $('.app-header').html();
        }, 'view to get populated', 1000);

        runs(function () {
            expect($('button.add-car')).toExist();
        });
    });

    it('should call add car action when add-car button clicked', function () {
        waitsFor(function () {
            return $('.app-header').html();
        }, 'view to get populated', 1000);

        runs(function () {
            $('button.add-car').trigger('click');
            expect(headerView.addVehicle).toHaveBeenCalled();
        });
    });
});